import java.util.*;
import java.io.*;
// gross : (
class Util {
    public static <T> void printStringItems(Iterable<T> items) {
        for (T f : items) {
            System.out.println("\t"+f.toString());
        }        
    }
    
    public static void printStringItemsWithNumbers(List<String> items) {
        if(items.size() <= 0) {
            System.out.println("None");
            return;
        }
        for (int i = 0; i < items.size(); ++i) {
            System.out.println("\t"+(i+1)+". "+items.get(i));
        }        
    }
    
    public static void fileWriter(String fileName, String content) throws FileNotFoundException, IOException {
        PrintWriter pw = new PrintWriter(fileName);
        pw.println(content);
        pw.close();
    }
}