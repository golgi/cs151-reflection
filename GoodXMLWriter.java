class GoodXMLWriter {
    private static String spaceGen(int n) {
        return String.format("%"+n+"s", "");
    }
    
    public static String xmlNode(String heading, String data, int spaces) {
        return spaceGen(spaces)+"<"+heading+">"+data+"</"+heading+">\n";
    }
    
    public static String xmlNode(String heading, String data) {
        return xmlNode(heading, data, 4);
    }
}