import java.util.*;
import java.lang.reflect.*;
import java.lang.*;
import java.util.zip.*;
import java.io.*;
import java.nio.file.*;
import java.nio.file.NotDirectoryException;
import java.io.FileNotFoundException; 
import java.net.*;

class PackageExplorer {
    protected PackageExplorer() {
        
    }
    
    public PackageExplorer(String directory) throws NotDirectoryException, MalformedURLException, ClassNotFoundException, DataFormatException, IOException {
        this.currentDirectory = directory;
        
        // return exception if string is not actually a directory
        if(!(new File(this.currentDirectory)).isDirectory()) {
            throw new NotDirectoryException(this.currentDirectory);
        }
        
        // normal initialization
        this.classes = this.loadClasses();
        this.classInfos = this.loadClassInfos();
        this.classStrings = this.loadClassStrings(this.classInfos);
    }
    
    protected List<String> loadClassStrings(List<ClassInfo> cilist) {
        List<String> cstrings = new ArrayList<>();
        for (ClassInfo c : cilist) {
            cstrings.add(c.getName());
        }       
        return cstrings;
    }
    
    protected List<Class<?>> loadClasses() throws MalformedURLException, ClassNotFoundException { 
        List<Class<?>> classlist = new ArrayList<>();
        System.out.println("Gathering class files in " + this.currentDirectory);
        FilenameFilter classFilter = new FilenameFilter() {
            public boolean accept(File dir, String name) {
                return name.toLowerCase().endsWith(".class");
            }
        };
        File f = new File(this.currentDirectory);
        URL url = f.toURI().toURL();
        URL[] urls = new URL[]{url};
        
        ClassLoader cl = new URLClassLoader(urls);
        for (File file : f.listFiles(classFilter) ) {
            Class cls = cl.loadClass(file.getName().substring(0, file.getName().length()-6));
            classlist.add(cls);
        }
        return classlist;
    }
    
    protected List<ClassInfo> loadClassInfos() {
        List<ClassInfo> classinfolist = new ArrayList<>();
        for(Class<?> c : this.classes) {
            classinfolist.add(new ClassInfo(c, this.classes));
        }
        return classinfolist;
    }
    
    public List<Class<?>> getClasses() {
        return this.classes;
    }
    
    public List<ClassInfo> getClassInfos() {
        return this.classInfos;
    }
    
    public List<String> getClassesAsStrings() {
        return this.classStrings;
    }

    public ClassInfo get(int index) {
        return this.classInfos.get(index);
    }
    
    public String getCurrentDirectory() {
        return this.currentDirectory;
    }
    
    public String generateClassesXML() throws IOException {
        StringBuilder classesXML = new StringBuilder("<PackageExplorer>\n<directory>"+this.currentDirectory+"</directory>\n<ClassList>");
        for (ClassInfo ci : this.classInfos) {
            classesXML.append(ci.generateXML());
        }
        return classesXML.append("</ClassList></PackageExplorer>").toString();
    }
    
    protected String currentDirectory;
    protected List<Class<?>> classes;
    protected List<ClassInfo> classInfos;
    protected List<String> classStrings;
}
