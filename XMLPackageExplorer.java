import java.util.*;
import java.lang.reflect.*;
import java.lang.*;
import java.util.zip.*;
import java.io.*;
import java.nio.file.*;
import java.nio.file.NotDirectoryException;
import java.io.FileNotFoundException; 
import java.net.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.*;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

class XMLPackageExplorer extends PackageExplorer {
    public XMLPackageExplorer(String s) throws NotDirectoryException, MalformedURLException, ClassNotFoundException, DataFormatException, IOException, SAXException, ParserConfigurationException {
        super();
        // open xml file
        this.classInfos = new ArrayList<>();
        File fxml = new File(s);
        DocumentBuilderFactory dbFactory =  DocumentBuilderFactory.newInstance();
        DocumentBuilder db = dbFactory.newDocumentBuilder();
        Document doc = db.parse(fxml);
        doc.getDocumentElement().normalize();
        if (!doc.getDocumentElement().getNodeName().equals("PackageExplorer"))
            throw new DataFormatException("invalid xml");
        this.currentDirectory = doc.getElementsByTagName("directory").item(0).getTextContent();
        NodeList classInfoNodes = doc.getElementsByTagName("ClassList").item(0).getChildNodes();
        System.out.println(classInfoNodes.getLength());
        for (int i = 0; i < classInfoNodes.getLength(); ++i) {
            Node cl = classInfoNodes.item(i);
            ClassInfo cli = new ClassInfo(cl);
            if (cli != null && cli.getName() != null)
                this.classInfos.add(cli);
        }
        this.classStrings = this.loadClassStrings(this.classInfos);
    }
}