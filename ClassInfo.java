import java.util.*;
import java.lang.reflect.*;
import java.lang.*;
import java.util.zip.*;
import java.io.*;
import java.nio.file.*;
import java.nio.file.NotDirectoryException;
import java.io.FileNotFoundException; 
import java.net.*;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import java.io.File;

class ClassInfo {
    private String className;
    private String superclassName;
    private List<String> interfaceNames;
    private List<Field> fields;
    private List<Method> methods;
    private Set<String> fieldNames;
    private Set<String> methodNames;
    private Set<String> providerNames;
    private Set<String> clientNames;

    // constructor to read from a java Class object
    public ClassInfo(Class<?> c) {
        this.className = c.getName();
        this.superclassName = c.getSuperclass() == null ? "none" : c.getSuperclass().getName();
        
        this.interfaceNames = new ArrayList<>();
        for (Class<?> i : c.getInterfaces()) {
            this.interfaceNames.add(i.getName());
        }
        this.fields = Arrays.asList(c.getDeclaredFields());
        this.methods = Arrays.asList(c.getDeclaredMethods());
        
        this.fieldNames = this.fieldsToStrings(this.fields);
        this.methodNames = this.methodsToStrings(this.methods);
        
        // default uninitialized
        this.providerNames = new TreeSet<>();
        this.clientNames = new TreeSet<>();
    }
    
    // construct from XML data
    public ClassInfo(Node xmlNode) throws NotDirectoryException, MalformedURLException, ClassNotFoundException, DataFormatException, IOException {
        this.interfaceNames = new ArrayList<>();
        this.providerNames = new TreeSet<>();
        this.clientNames = new TreeSet<>();
        this.fieldNames = new TreeSet<>();
        this.methodNames = new TreeSet<>();
        NodeList classInfoNodes = xmlNode.getChildNodes();
        for (int i = 0; i < classInfoNodes.getLength(); ++i) {
            String nam = classInfoNodes.item(i).getNodeName();
            String val = classInfoNodes.item(i).getTextContent();
            if (nam.equals("name")) 
                this.className = val;
            else if (nam.equals("superclass"))
                this.superclassName = val;
            else if (nam.equals("interface"))
                this.interfaceNames.add(val); 
            else if (nam.equals("field"))
                this.fieldNames.add(val);            
            else if (nam.equals("method"))
                this.methodNames.add(val);            
            else if (nam.equals("provider"))
                this.providerNames.add(val);            
            else if (nam.equals("client"))
                this.clientNames.add(val);
        }
    }
    
    
    public ClassInfo(Class<?> c, List<Class<?>> inDirectoryClasses) {
        this(c);
        this.loadProviders(inDirectoryClasses);
        this.loadClients(inDirectoryClasses);
    }
    
    private void loadProviders(List<Class<?>> inDirectoryClasses) {
        this.providerNames = new TreeSet<String>();
        Class<?> ty;
        for (Field f : this.fields) {
            ty = f.getType();
            if (inDirectoryClasses.contains(ty) && !ty.getName().equals(this.className)) {
                this.providerNames.add(f.getType().getName());
            }
        }
        for (Method m : this.methods) {
            for (Class<?> t : m.getParameterTypes()) {
                if (inDirectoryClasses.contains(t) && !t.getName().equals(this.className)) {
                    this.providerNames.add(t.getName());
                }
            }
        }
    }
    
    private void loadClients(List<Class<?>> inDirectoryClasses) {
        this.clientNames = new TreeSet<String>();
        Class<?> ty;
        for (Class<?> c : inDirectoryClasses) {
            for (Field f : c.getFields()) {
                ty = f.getType();
                if (inDirectoryClasses.contains(ty) && !ty.getName().equals(this.className) && f.getType().equals(c)) {
                    this.clientNames.add(f.getType().getName());
                }
            }
            for (Method m : c.getMethods()) {
                for (Class<?> t : m.getParameterTypes()) {
                    if (t.equals(c) && inDirectoryClasses.contains(t) && !t.getName().equals(this.className))
                        this.clientNames.add(t.getName());
                }
            }
        }
    }
    
    // even though both Field and Method are AccessibleObjects, there is no getName method in that parent class : (
    private Set<String> fieldsToStrings(List<Field> fieldList) {
        Set<String> flist = new TreeSet<>();
        for (Field f : fieldList) {
            flist.add(f.getName());
        }
        return flist;
    }

   
    private Set<String> methodsToStrings(List<Method> methodList) {
        Set<String> flist = new TreeSet<>();
        for (Method f : methodList) {
            flist.add(f.getName());
        }
        return flist;
    }
    
    public void writeConsole() {
        System.out.println("Class Details:");
        System.out.println("Name: "+this.className);
        System.out.println("Superclass: "+this.superclassName);
        System.out.print("Interfaces: ");
        if(this.interfaceNames.size() <= 0) {
            System.out.print("None");
        } else {
            for(String i : this.interfaceNames) {
                System.out.print(i+" ");
            }
        }
        System.out.println("\nFields: ");
        Util.printStringItems(this.fieldNames);
        System.out.println("Methods: ");
        Util.printStringItems(this.methodNames);
        
        if (this.providerNames != null){
            System.out.println("Providers: ");
            Util.printStringItems(this.providerNames);
        }
        if (this.clientNames != null){
            System.out.println("Clients: ");
            Util.printStringItems(this.clientNames);     
        }            
    }
    
    public String getName() {
        return this.className;
    }
    
    
    // generate an XML document from the class data
    public String generateXML() throws IOException{
        StringBuilder classInfoXML = new StringBuilder("<ClassInfo>\n");
        classInfoXML.append(GoodXMLWriter.xmlNode("name", this.className));
        classInfoXML.append(GoodXMLWriter.xmlNode("superclass", this.superclassName));
        for (String i : this.interfaceNames) {
            classInfoXML.append(GoodXMLWriter.xmlNode("interface", i));
        }
        for (String i : this.fieldNames) {
            classInfoXML.append(GoodXMLWriter.xmlNode("field", i));
        }
        for (String i : this.methodNames) {
            classInfoXML.append(GoodXMLWriter.xmlNode("method", i));
        }
        for (String i : this.providerNames) {
            classInfoXML.append(GoodXMLWriter.xmlNode("provider", i));
        }
        for (String i : this.clientNames) {
            classInfoXML.append(GoodXMLWriter.xmlNode("client", i));
        }
        return classInfoXML.append("</ClassInfo>\n").toString();
    }
}
