import java.util.*;
import java.lang.reflect.*;
import java.lang.*;
import java.util.zip.*;
import java.io.*;
import java.nio.file.*;
import java.nio.file.NotDirectoryException;
import java.io.FileNotFoundException; 
import java.net.*;

public class PackageExplorerClient {
    
    public static int choicePicker(int totalChoices) throws IOException {
        int choice = 0;
        System.out.println("Enter a choice from 1-"+totalChoices+": ");        
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        do {
            try {
                choice = Integer.parseInt(br.readLine());
            } 
            catch (Exception e) {
                System.err.println("Input is invalid, please try again: ");
            }
        } while (choice == 0);
        return choice;
    }
    
    public static int mainMenuSelector() throws IOException {
        System.out.println("Main Menu\n-------------------------------");
        System.out.println("    \t1. List all classes");
        System.out.println("    \t2. View a class");
        System.out.println("    \t3. Save all classes to XML");
        System.out.println("    \t4. Load class info from XML");
        System.out.println("    \t5. QUIT");

        return choicePicker(5);
    }
    
    public static void classInfoViewer(ClassInfo ci) throws IOException {
        ci.writeConsole();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println("Do you want to save this file into an XML? (y/n)");
        String conf = br.readLine();
        if(conf.equals("Y") || conf.equals("y")) {
            Util.fileWriter(ci.getName()+".xml", ci.generateXML());
        }
    }    
    
    public static void main(String[] args) throws IOException, ClassNotFoundException, MalformedURLException, DataFormatException {
        PackageExplorer pe = new PackageExplorer(args.length < 1 ? System.getProperty("user.dir") : args[0]);
        PackageExplorer old_pe = pe;
        System.out.println("Welcome to the Package Explorer!");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        ClassInfo ci;
        int classIndex = -1;
        int choice = 0;
        do {
            System.out.println("Current directory: "+pe.getCurrentDirectory());
            choice = mainMenuSelector();
            List<String> classList = pe.getClassesAsStrings();
            
            switch (choice) {
                
                // list each class, maybe view one (and maybe save to XML)
                case 1:
                    Util.printStringItemsWithNumbers(classList);
                    System.out.println("\n\t"+(classList.size()+1)+". RETURN TO MAIN MENU");
                    classIndex = choicePicker(classList.size()+1)-1; 
                    if (classIndex < classList.size()) 
                        classInfoViewer(pe.get(classIndex));

                    else if (classIndex > classList.size()) 
                        System.out.println("Invalid input...returning to main menu");
                    break; 
                    
                // view a class (and maybe save to XML)
                case 2:
                    System.out.println("Please enter the name of the class you wish to view: ");
                    classIndex = classList.indexOf(br.readLine());
                    if (classIndex != -1) 
                        classInfoViewer(pe.get(classIndex));
                    else
                        System.out.println("Invalid input...returning to main menu");
                    break;
                    
                // save all to XML
                case 3:
                    System.out.println("Enter a filename to SAVE: ");
                    Util.fileWriter(br.readLine(), pe.generateClassesXML());
                    break;
                    
                // wipe out current packageexplorer data and load all from from XML
                case 4:
                    System.out.println("Enter a filename to READ: ");
                    try {
                        pe = new XMLPackageExplorer(br.readLine());
                        old_pe = pe; 
                    }
                    catch (FileNotFoundException e) {
                        System.out.println("FileNotFoundException: XML File not found. Reverting to previous package explorer...");
                        pe = old_pe;
                    }
                    catch (DataFormatException e) {
                        System.out.println("DataFormatException: File is an Invalid XML file. Reverting to previous package explorer...");
                        pe = old_pe;
                    } 
                    catch (Exception e) {
                        pe = old_pe;
                    }
                    break;     
            }
        } while (choice > 0 && choice < 5);
    }
}
